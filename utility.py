"""
Transverse module

This module defines the transverse services usable by all the layers

Functions:
     - getconfigs: access to the configuration file
     - getlog(nm): access to the log file
     - debug(name): definition of the Debug decorator
"""

import functools
import logging

from jproperties import Properties


def getconfigs():
    """
    Return properties
    :return: properties
    :rtype: Properties

    >>> getconfigs().get('UPLOAD_FOLDER').data
    './temp'
    """
    configuration = Properties()
    try:
        with open("app-config.properties", "rb") as config_file:
            configuration.load(config_file)
    finally:
        config_file.close()
    return configuration


configs = getconfigs()


def getlog(file_name):
    """
    Return logger for log information
    :param file_name: name of python file
    :return: logger
    :rtype: Logger
    """
    logger = logging.getLogger(file_name)
    try:
        with open("app-config.properties", "rb") as config_content:
            loglevel = configs.get("LOG_LEVEL").data
        if loglevel == "DEBUG":
            logger.setLevel(logging.DEBUG)
        elif loglevel == "INFO":
            logger.setLevel(logging.INFO)
        elif loglevel == "WARNING":
            logger.setLevel(logging.WARNING)
        elif loglevel == "ERROR":
            logger.setLevel(logging.ERROR)
        elif loglevel == "CRITICAL":
            logger.setLevel(logging.CRITICAL)
    finally:
        config_content.close()

    formatter = \
        logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(" "message)s")
    file_handler = logging.FileHandler(configs.get("LOGGER_PATH").data)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    return logger


def debug(name):
    """
    decorator for debug

    :param name:
    :return: void
    """

    def debug_decorator(function):
        @functools.wraps(function)
        def logger_debug(*args, **kwargs):
            logger = getlog(name)
            logger.debug("Call function : %s", function.__name__)
            return function(*args, **kwargs)

        return logger_debug

    return debug_decorator
