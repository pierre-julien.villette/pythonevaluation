"""
ModuleView

This abstract module defines function signatures

Functions:
    - format_describe_file(status, metadata, link): formats the
    description of the file
    - format_upload_file(id): formats the response to a file upload
    - format_extracted_text(text): formats the content of a file
"""

from abc import ABC, abstractmethod


class ViewResponse(ABC):
    """
     Abstract class for response formatting

     Methods :
     - format_describe_file(status, metadatas, link): formats the
     description of the file
     - format_upload_file(id): formats the response to a file upload
     - format_extracted_text(text): formats the content of a file
    """

    @abstractmethod
    def format_describe_file(self, status, metadatas, link):
        """
        formats the response
        :rtype: str
        :param: status:
                metadata:
                link:
        :return: formatted response
        """
        return NotImplemented

    @abstractmethod
    def format_upload_file(self, identifiant):
        """
        formats the response
        :rtype: str
        :param identifiant:
        :return: formatted response
        """

        return NotImplemented

    @abstractmethod
    def format_extracted_text(self, text):
        """
        formats the response

        :rtype: str
        :param text:
        :return: formatted response
        """
        return NotImplemented
