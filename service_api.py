"""
Service module

This module defines the APIs offered

Functions:
     - describe_file(id): description of a file
     - upload_file(): file upload
     - read_extracted_text(id): reading the extracted content of a file
     - delete_file(id): deletion of a file
"""
import os

from flask import Flask, abort, redirect, request
from werkzeug.utils import secure_filename

import service_pdf as sp
import utility
from view_response_html import ViewResponseHtml
from view_response_json import ViewResponseJson
from view_response_xml import ViewResponseXml

ALLOWED_EXTENSIONS = {"pdf"}
ALLOWED_TYPES_MIME = {"application/pdf"}

configs = utility.getconfigs()
logger = utility.getlog(__name__)

app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = configs.get("UPLOAD_FOLDER").data
app.config["MAX_CONTENT_LENGTH"] = eval(configs.get("MAX_CONTENT_LENGTH").data)


@utility.debug(__name__)
def allowed_file(file):
    """
    Check if file type is allowed.
    :param file:
    :return: True if file is allowed
    :rtype: bool

    >>> allowed_file(None)
    Traceback (most recent call last):
        ...
    ValueError: No file found
    """
    try:
        filename = file.filename
        check_extension = (
            "." in filename and filename.rsplit(".", 1)[1].lower()
            in ALLOWED_EXTENSIONS
        )
        check_type = file.mimetype in ALLOWED_TYPES_MIME
        return check_type and check_extension
    except Exception as error:
        raise ValueError("No file found") from error


def __get_ip_from(request_user):
    """
    return ip from the request
    :param request_user:
    :return: ipFrom
    :rtype: str
    """
    try:
        return request_user.remote_addr
    except (RuntimeError, AttributeError) as error:
        logger.error("An error occurred: %s", error)
        return "unknow"


def __get_view_response(request_user):
    """
    formats the response according the parameter 'Accept'.
    :param request_user:
    :return: view_response, type
    :rtype: str, str
    """
    accept = request_user.headers["Accept"]

    if accept == "application/xml":
        view_response = ViewResponseXml()
        type_accept = {"Content-Type": "application/xml"}
    elif accept == "application/json":
        view_response = ViewResponseJson()
        type_accept = {"Content-Type": "application/json"}
    else:
        view_response = ViewResponseHtml()
        type_accept = {"Content-Type": "text/html"}
    return view_response, type_accept


@app.route("/documents/<identifiant>", methods=["GET"])
def describe_file(identifiant):
    """
    describe the document corresponding to the id.
    API REST GET
    :rtype: objet
    :param identifiant:
    :return: list of metadata and link to its text content

    >>> describe_file('-1')
    Traceback (most recent call last):
        ...
    werkzeug.exceptions.PreconditionFailed: 412 Precondition Failed: id must be positive int
    """
    logger.info("Call function : %s by %s", "describe_file",
                __get_ip_from(request))
    try:
        if not identifiant.isnumeric():
            raise ValueError("id must be positive int")
        view_response, type_accept = __get_view_response(request)
        response = view_response.format_describe_file(
            sp.read_status(identifiant),
            sp.read_metada(identifiant),
            request.host_url + "text/" + identifiant + ".txt",
        )
        return response, type_accept

    except ValueError as error:
        abort(412, description=error)
        return redirect(request.url)


@app.route("/documents", methods=["POST"])
def upload_file():
    """
    upload the pdf file passed as a parameter
    API REST POST
    :rtype: object
    :return: document record ID

    >>> upload_file() # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
    RuntimeError:
    """
    logger.info("Call function : %s by %s", "upload_file",
                __get_ip_from(request))
    try:
        if "file" not in request.files:
            raise ValueError("The parameter: file is empty")

        file = request.files["file"]
        if file.filename == "":
            raise ValueError("The parameter: file is not completed")
        if file and allowed_file(file):
            identifiant = sp.create_pdf(file.filename)
            logger.info("Création %s (id:%d)", file.filename, identifiant)
            filename = secure_filename(file.filename)
            if not os.path.isdir(app.config["UPLOAD_FOLDER"]):
                os.makedirs(app.config["UPLOAD_FOLDER"])
            file_path = os.path.join(app.config["UPLOAD_FOLDER"], filename)
            file.save(file_path)
            file.close()

            try:
                sp.process_pdf(file_path, identifiant)
                logger.info("Fin du traitement de %s (id:%d)", file_path,
                            identifiant)
            finally:
                os.remove(file_path)

            view_response, type_accept = __get_view_response(request)
            response = view_response.format_upload_file(identifiant)

        else:
            raise ValueError("The type of file is not accepted")

        return response, type_accept

    except ValueError as error:
        logger.error(error)
        abort(412, description=error)
        return redirect(request.url)


@app.route("/text/<identifiant>.txt", methods=["GET"])
def read_extracted_text(identifiant):
    """
    read the extracted text the document corresponding to the id.
    API REST GET
    :rtype: objet
    :param identifiant:
    :return: extracted text

    >>> read_extracted_text('-1')
    Traceback (most recent call last):
        ...
    werkzeug.exceptions.PreconditionFailed: 412 Precondition Failed: id must be positive int
    """
    logger.info(
        "Call function : %s by %s", "read_extracted_text",
        __get_ip_from(request)
    )
    try:
        if not identifiant.isnumeric():
            raise ValueError("id must be positive int")

        text = sp.read_txt(identifiant)
        view_response, type_accept = __get_view_response(request)
        response = view_response.format_extracted_text(text)

        return response, type_accept
    except ValueError as error:
        abort(412, description=error)
        return redirect(request.url)


@app.route("/documents/<identifiant>", methods=["DELETE"])
def delete_file(identifiant):
    """
    delete the document corresponding to the id.
    API REST DELETE
    :rtype: objet
    :param identifiant:
    :return: confirmation

    >>> delete_file('-1')
    Traceback (most recent call last):
        ...
    werkzeug.exceptions.PreconditionFailed: 412 Precondition Failed: id must be positive int
    """
    logger.info("Call function : %s by %s", "delete_file",
                __get_ip_from(request))
    try:
        if not identifiant.isnumeric():
            raise ValueError("id must be positive int")

        if sp.delete_pdf(identifiant):
            text = f"Le document {identifiant} a été supprimé"
            view_response, type_accept = __get_view_response(request)
            response = view_response.format_extracted_text(text)
            return response, type_accept

        return None

    except ValueError as error:
        abort(412, description=error)
        return redirect(request.url)
