"""
Module Model

This module defines the FilePDF and Metadata classes and ensures the
persistence of their instances
"""

from sqlalchemy import Column, ForeignKey, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

import utility

configs = utility.getconfigs()
engine_echo = configs.get("LOG_LEVEL").data == "DEBUG"

engine = create_engine("sqlite:///" + configs.get("DB_PATH").data,
                       echo=engine_echo)
Session = sessionmaker(bind=engine)
session = Session()

Base = declarative_base()


class FilePDF(Base):
    """
    Classe du fichier PDF

    Attributs :
        id (int) : numéro d'identifiant
        processing_state (String): Etat du processus
        title (String): titre du fichier PDF
        text (String): contenu extrait du fichier
        metadatas (dict<String,String>): ensemble des métadatas

    Méthodes :
        néant
    """

    __tablename__ = "T_filePDF"
    id = Column(Integer, primary_key=True)
    processing_state = Column(String, default="pending")
    title = Column(String)
    text = Column(String)
    metadatas = relationship("Metadata")

    def __str__(self):
        """
        Concise string representation of FilePDF
        :rtype: str
        :return: representation of FilePDF
        """
        result = str(self.id) + "|" + self.title
        result += self.processing_state

        for metadata in self.metadatas:
            result += "\n" + str(metadata)

        result += "\n\n" + self.text

        return result

    def __repr__(self):
        """
        Concise string representation of FilePDF
        :rtype: str
        :return: representation of FilePDF
        """
        return self.__str__()


class Metadata(Base):
    """
        Classe d'une métadonnée

        Attributs :
            id (int) : numéro d'identifiant
            id_table (int): numéro d'identifiant d'une instance de FilePDF
            key (String): nom de la métadonnée
            value (String): valeur de la métadonnée

        Méthodes :
            néant
        """

    __tablename__ = "T_metadata"
    id = Column(Integer, primary_key=True)
    id_table = Column(Integer, ForeignKey("T_filePDF.id"))
    key = Column(String)
    value = Column(String)

    def __str__(self):
        """
        Concise string representation of Metadata
        :rtype: str
        :return: representation of Metadata
        """
        return self.key + " : " + self.value

    def __repr__(self):
        """
        Concise string representation of FilePDF
        :rtype: str
        :return: representation of FilePDF
        """
        return self.__str__()
