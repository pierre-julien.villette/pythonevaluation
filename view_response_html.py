"""
ModuleView

This module implements the functions of the view_response interface with an
html format

Functions:
     - format_describe_file(status, metadata, link): formats the description of
      the file
     - format_upload_file(id): formats the response to a file upload
     - format_extracted_text(text): formats the content of a file
"""

from view_response import ViewResponse


class ViewResponseHtml(ViewResponse):
    """
      Implementation of the ViewResponse abstract class for an HTML format

      Methods:
          - format_describe_file(status, metadata, link): formats the
          description of the file
          - format_upload_file(id): formats the response to a file upload
          - format_extracted_text(text): formats the content of a file
    """

    @classmethod
    def __format_html(cls, txt):
        """
        formats the response in html format
        :rtype: str
        :param: txt
        :return: formatted response
        """
        return "<h1>" + txt + "</h1>"

    def format_describe_file(self, status, metadatas, link):
        """
        formats the response
        :rtype: str
        :param: status:
                metadata:
                link:
        :return: formatted response
        """
        output = "Description du pdf"
        output += "Status : " + status
        for metadata in metadatas:
            output += "\n" + str(metadata)
        output += "\n\nContenu du pdf : " + link + "\n"
        return output

    def format_upload_file(self, identifiant):
        """
        formats the response
        :rtype: str
        :param identifiant:
        :return: formatted response
        """
        return self.__format_html("PDF enregistré sour l'id : "
                                  + str(identifiant))

    def format_extracted_text(self, text):
        """
        formats the response

        :rtype: str
        :param text:
        :return: formatted response
        """
        return self.__format_html(text)
