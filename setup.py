"""
This module allows the generation of a .tar.gz archive from the command:
python setup.py sdist

To install the application, it will be enough to use the command:
pip install python_eval-XX.tar.gz
"""
from setuptools import find_packages, setup

setup(
    name="PYTHON_EVAL",
    version="1.0",
    packages=find_packages(),
    url="https://gitlab-student.centralesupelec.fr/pierre-julien.villette"
    "/pythonevaluation",
    license="MIT",
    author="VILLETTE",
    author_email="pierre-julien.villette@student-cs.fr",
    description="API réalisée dans le cadre de l'évolution du cours"
    " de Python de MSI-SIO 2022-2023. Elle permet "
    "l'extraction du texte contenu dans un fichier PDF.",
    install_requires=[
        "pdfminer==20191125",
        "pdfminer.six==20220524",
        "pytest==7.2.0",
        "pytest-cov==4.0.0",
        "pytest-xdist==3.0.2",
        "setuptools~=60.2.0",
        "jproperties~=2.1.1",
        "SQLAlchemy~=1.4.42",
        "Werkzeug~=2.2.2",
        "Flask~=2.2.2",
        "pdfrw~=0.4",
        "bs4~=0.0.1",
        "beautifulsoup4~=4.11.1",
    ],
)
