"""
ModuleView

This module implements the functions of the view_response interface with an xml
 format

Functions:
     - format_describe_file(status, metadata, link): formats the description of
      the file
     - format_upload_file(id): formats the response to a file upload
     - format_extracted_text(text): formats the content of a file
"""
from bs4 import BeautifulSoup as bs

from view_response import ViewResponse


class ViewResponseXml(ViewResponse):
    """
      Implementation of the ViewResponse abstract class for an XML format

      Methods:
          - format_describe_file(status, metadata, link): formats the
          description of the file
          - format_upload_file(id): formats the response to a file upload
          - format_extracted_text(text): formats the content of a file
    """

    def format_describe_file(self, status, metadatas, link):
        """
        formats the response
        :rtype: str
        :param: status:
                metadata:
                link:
        :return: formatted response
        """

        xml = bs(features="xml")
        describe_file = xml.new_tag("DescribeFile")
        status = xml.new_tag("status")
        status.string = status
        describe_file.append(status)

        for metadata in metadatas:
            tag = xml.new_tag(str(metadata.key))
            tag.string = str(metadata.value)
            describe_file.append(tag)

        tag_link = xml.new_tag("link")
        tag_link.string = link
        describe_file.append(tag_link)

        xml.append(describe_file)

        return xml.prettify()

    def format_upload_file(self, identifiant):
        """
        formats the response
        :rtype: str
        :param identifiant:
        :return: formatted response
        """

        xml = bs(features="xml")
        upload_file = xml.new_tag("UploadFile")
        tag = xml.new_tag("id")
        tag.string = str(identifiant)
        upload_file.append(tag)
        xml.append(upload_file)

        return xml.prettify()

    def format_extracted_text(self, text):
        """
        formats the response

        :rtype: str
        :param text:
        :return: formatted response
        """
        xml = bs(features="xml")
        upload_file = xml.new_tag("UploadFile")
        tag = xml.new_tag("text")
        tag.string = text
        upload_file.append(tag)
        xml.append(upload_file)

        return xml.prettify()
