# API Reference

---
__Endpoint :__ /documents

__Méthode :__ POST

__Description :__ dépôt d'un fichier

__Paramètres :__

| Name | Description | 
|------|-------------------|
| file | Fichier à déposer |

__Contraintes techniques :__

| Type              | Valeur | 
|-------------------|--------|
| file size limit   | 16 Mo  |
| file type allowed | PDF    |

__Accept :__ application/json ; application/xml

__Requête :__
```
> POST /documents HTTP/1.1
> Host: 127.0.0.1:5000
> User-Agent: curl/7.82.0
> Accept: */*
> Content-Length: 8380117
> Content-Type: multipart/form-data; boundary=------------------------48bb9bcb6ac79b6c
> Expect: 100-continue
>
```

__Réponse :__
```
< HTTP/1.0 200 OK
< Content-Type: text/html
< Content-Length: 39
< Server: Werkzeug/2.0.3 Python/3.9.12
< Date: Thu, 26 Jan 2023 21:08:57 GMT
<
<h1>PDF enregistré sour l'id : 28</h1>%
```
---
__Endpoint :__ /documents/{id}

__Méthode :__ GET

__Description :__ description de fichier

__Paramètres :__

| Name | Description             | 
|------|-------------------------|
| id   | Identifiant du document |

__Accept :__ application/json ; application/xml

__Requête :__
```
> GET /documents/27 HTTP/1.1
> Host: 127.0.0.1:5000
> User-Agent: curl/7.82.0
> Accept: */*
>
```

__Réponse :__
```
< HTTP/1.0 200 OK
< Content-Type: text/html
< Content-Length: 420
< Server: Werkzeug/2.0.3 Python/3.9.12
< Date: Thu, 26 Jan 2023 21:13:35 GMT
<
Description du pdfStatus : success
Author : Yves
CreationDate : D:20101014140734+02'00'
Creator : Adobe InDesign CS4 \(6.0.5\)
Keywords :
ModDate : D:20101130141318+01'00'
Producer : Adobe PDF Library 9.0; modified using iText 5.0.1 \(c\) 1T3XT BVBA
Title : Expression des besoins pour le système d'information
Trapped : Unknow
EBX_PUBLISHER : Eyrolles

Contenu du pdf : http://127.0.0.1:5000/text/27.txt
```
___
__Endpoint :__ /text/{id}.txt

__Méthode :__ GET

__Description :__ lecture du texte extrait du fichier

__Paramètres :__

| Name | Description             | 
|------|-------------------------|
| id   | Identifiant du document |

__Accept :__ application/json ; application/xml

__Requête :__
```
> GET /text/29.txt HTTP/1.1
> Host: 127.0.0.1:5000
> User-Agent: curl/7.82.0
> Accept: */*
>
```

__Réponse :__
```
< HTTP/1.0 200 OK
< Content-Type: text/html
< Content-Length: 36
< Server: Werkzeug/2.0.3 Python/3.9.12
< Date: Fri, 27 Jan 2023 07:46:08 GMT
<
<h1>Ceci est un pdf de test
</h1>
```
___
__Endpoint :__ /documents/{id}

__Méthode :__ DELETE

__Description :__ suppression d'un fichier

__Paramètres :__

| Name | Description             | 
|------|-------------------------|
| id   | Identifiant du document |

__Accept :__ application/json ; application/xml

__Requête :__
```
> DELETE /documents/27 HTTP/1.1
> Host: 127.0.0.1:5000
> User-Agent: curl/7.82.0
> Accept: */*
>
```

__Réponse :__
```
< HTTP/1.0 200 OK
< Content-Type: text/html
< Content-Length: 41
< Server: Werkzeug/2.0.3 Python/3.9.12
< Date: Fri, 27 Jan 2023 07:51:07 GMT
<
<h1>Le document 27 a été supprimé</h1>
```
___