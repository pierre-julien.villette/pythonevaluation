# PYTHON Evaluation
_Pierre-Julien VILLETTE, le 31/01/2023_

## Présentation générale
Cette application propose une API permettant l'extraction du texte contenu dans un document PDF.

## Services proposés

![Diagramme des cas d'utilisation](pictures/diag_uc.png)

L'API propose 4 actions pour le service d'extraction du texte contenu dans le PDF:
- [ ] __upload file:__ Action préalable à l'extraction, le dépôt du fichier PDF est confirmé par la génération de l'identifiant correspondant;  
- [ ] __read extracted file:__ Cette action envoie le contenu du fichier PDF soit format texte;
- [ ] __describe file:__ La description du fichier comprend les métadonnées (titre, auteur...) et le status du traitement de l'extraction (pending, success, failure);
- [ ] __delete file:__ Le traitement du fichier étant sauvegardé, il est possible de le supprimer.

## Contrat API

voir _API_Reference.md_

## Installation

```
pip install PYTHON_EVAL-1.0.tar.gz
git clone https://gitlab-student.centralesupelec.fr/pierre-julien.villette/pythonevaluation/
```

### Mise en service
Depuis le dossier PYTHON_EVAL/, lancer la commande:

```
FLASK_APP=service_api.py flask run
```

### Utilisation du service
Pour utiliser l'application, ouvrez un terminal et utilisez les commandes suivantes

#### dépôt d'un fichier:
_ici le chemin d'accès du fichier est /Users/vpj/Desktop/test.pdf :_ 

```
curl -X POST -F "file=@/Users/vpj/Desktop/test.pdf" http://127.0.0.1:5000/documents
curl -H "Accept: application/json" -X POST -F "file=@/Users/vpj/Desktop/test.pdf" http://127.0.0.1:5000/documents
curl -H "Accept: application/xml" -X POST -F "file=@/Users/vpj/Desktop/test.pdf" http://127.0.0.1:5000/documents
```

#### description de fichier:
_ici l'identifiant du fichier est 1_

```
curl http://127.0.0.1:5000/documents/1
curl -H "Accept: application/json" http://127.0.0.1:5000/documents/1
curl -H "Accept: application/xml" http://127.0.0.1:5000/documents/1
```

#### lecture du texte extrait du fichier:
_ici l'identifiant du fichier est 1_

```
curl http://127.0.0.1:5000/text/1.txt
curl -H "Accept: application/json" http://127.0.0.1:5000/text/1.txt
curl -H "Accept: application/xml" http://127.0.0.1:5000/text/1.txt
```

#### suppression d'un fichier:
_ici l'identifiant du fichier est 1_

```
curl -X DELETE http://127.0.0.1:5000/documents/1
```

## Organisation

![Diagramme de classes](pictures/diag_pkg.png)
![Diagramme de séquence upload](pictures/diag_upload_file.png)
![Diagramme de séquence describe](pictures/diag_describe_file.png)
![Diagramme de séquence read](pictures/diag_read_text.png)
![Diagramme de séquence delete](pictures/diag_delete_file.png)
![Diagramme d_etat_FilePDF](pictures/diag_statemachine.png)


## Exigences

- [ ] Le fichier déposé doit être au format PDF
- [ ] Le fichier doit avoir une taille inférieure ou égale à 16 Mo
- [ ] La réponse est au format json, xml ou html

## Test de l'application

### avec pytest
Depuis le répertoire PYTHON_EVAL/test/:

```
pytest *_test.py
```

### avec doctest
Depuis le répertoire PYTHON_EVAL/:
```
python -m doctest -v *.py
```
---
