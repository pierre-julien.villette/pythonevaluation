import unittest
import sys
sys.path.append('../')
import service_api as sa


class ServiceApiTest(unittest.TestCase):

    def test_upload_file(self):
        with sa.app.test_client() as c:
            rv = c.post('/documents',
                        data={'file': ('./test1.pdf', 'test1.pdf')},
                        headers={'Accept': 'application/json'})
            response_status = str(rv.status)
            self.assertRegex(response_status, '^200')

    def test_upload_wrong_file(self):
        with sa.app.test_client() as c:
            rv = c.post('/documents',
                        data={'file': ('./test2.docx', 'test2.docx')},
                        headers={'Accept': 'application/json'})
            response_status = str(rv.status)
            self.assertRegex(response_status, '^412')

    def test_describe_file(self):
        with sa.app.test_client() as c:
            rv = c.get('/documents/1',
                       headers={'Accept': 'application/json'})
            response_status = str(rv.status)
            self.assertRegex(response_status, '^200')

    def test_describe_file_out_id(self):
        with sa.app.test_client() as c:
            rv = c.get('/documents/99999',
                       headers={'Accept': 'application/json'})
            response_status = str(rv.status)
            self.assertRegex(response_status, '^412')

    def test_describe_file_wrong_id(self):
        with sa.app.test_client() as c:
            rv = c.get('/documents/id',
                       headers={'Accept': 'application/json'})
            response_status = str(rv.status)
            self.assertRegex(response_status, '^412')

    def test_read_extracted_text(self):
        with sa.app.test_client() as c:
            rv = c.get('/text/1.txt',
                       headers={'Accept': 'application/json'})
            response_status = str(rv.status)
            self.assertRegex(response_status, '^200')

    def test_read_extracted_text_out_id(self):
        with sa.app.test_client() as c:
            rv = c.get('/text/99999.txt',
                       headers={'Accept': 'application/json'})
            response_status = str(rv.status)
            self.assertRegex(response_status, '^412')

    def test_read_extracted_text_wrong_id(self):
        with sa.app.test_client() as c:
            rv = c.get('/text/id.txt',
                       headers={'Accept': 'application/json'})
            response_status = str(rv.status)
            self.assertRegex(response_status, '^412')


if __name__ == '__main__':
    unittest.main()
