import unittest
import sys
sys.path.append('../')
import service_pdf as sp
import file_pdf as fp

session = fp.Session()
fp.Base.metadata.create_all(fp.engine)


class ServicePdfTest(unittest.TestCase):

    def test_create_pdf(self):
        nb_files = session.query(fp.FilePDF).count()
        id = sp.create_pdf('test1.pdf')
        self.assertTrue((type(id) == int) and (id > 0))
        self.assertTrue(sp.read_status(id) == 'pending')
        self.assertTrue(session.query(fp.FilePDF).count() == nb_files + 1)

    def test_process_pdf(self):
        id = sp.create_pdf('test1.pdf')
        f = sp.get_file_pdf(id)
        self.assertIsNone(f.text)
        self.assertTrue(len(f.metadatas) == 0)
        sp.process_pdf('test1.pdf', id)
        self.assertIsNotNone(f.text)
        self.assertTrue(len(f.metadatas) > 0)
        self.assertTrue(sp.read_status(id) == 'success')

    def test_read_txt(self):
        id = sp.create_pdf('test1.pdf')
        sp.process_pdf('test1.pdf', id)
        self.assertTrue(sp.read_txt(id) == 'Ceci est un pdf de test \n\n\x0c')

    def test_read_status(self):
        id = sp.create_pdf('test1.pdf')
        self.assertTrue(sp.read_status(id) == 'pending')
        sp.process_pdf('test1.pdf', id)
        self.assertTrue(sp.read_status(id) == 'success')
        id = sp.create_pdf('test2.docx')
        self.assertTrue(sp.read_status(id) == 'pending')
        sp.process_pdf('test2.docx', id)
        self.assertTrue(sp.read_status(id) == 'failure')


if __name__ == '__main__':
    unittest.main()
