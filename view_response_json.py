"""
ModuleView

This module implements the functions of the view_response interface with a json
 format

Functions:
     - format_describe_file(status, metadata, link): formats the description of
      the file
     - format_upload_file(id): formats the response to a file upload
     - format_extracted_text(text): formats the content of a file
"""

import json
from io import StringIO

from view_response import ViewResponse


class ViewResponseJson(ViewResponse):
    """
      Implementation of the ViewResponse abstract class for an JSON format

      Methods:
          - format_describe_file(status, metadata, link): formats the
          description of the file
          - format_upload_file(id): formats the response to a file upload
          - format_extracted_text(text): formats the content of a file
    """

    @classmethod
    def __format_json(cls, json_content):
        """
        formats the response in json format
        :rtype: str
        :param: js
        :return: formatted response
        """

        input_output = StringIO()
        json.dump(json_content, input_output, indent=2)
        return input_output.getvalue()

    def format_describe_file(self, status, metadatas, link):
        """
        formats the response
        :rtype: str
        :param: status:
                metadata:
                link:
        :return: formatted response
        """

        json_content = {"status": status}
        for metadata in metadatas:
            json_content[metadata.key] = metadata.value
        json_content["link"] = link

        return self.__format_json(json_content)

    def format_upload_file(self, identifiant):
        """
        formats the response
        :rtype: str
        :param identifiant:
        :return: formatted response
        """

        json_content = {"id": identifiant}

        return self.__format_json(json_content)

    def format_extracted_text(self, text):
        """
        formats the response

        :rtype: str
        :param text:
        :return: formatted response
        """

        json_content = {"text": text}

        return self.__format_json(json_content)
