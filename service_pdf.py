"""
Service module

This module defines the processing applicable to a pdf file

Functions:
     - create_pdf(filename): creation of a PDF file
     - process_pdf(file_pdf, id): extracting metadata and content
        from a PDF file
     - read_extracted_text(id): reading the extracted content of a file
     - get_file_pdf(id): search for a PDF file from its identifier
     - read_txt(id): reading the contents of a PDF file
     - read_metada(id): read metadata from a PDF file
     - read_status(id): reading the status of a PDF file
     - delete_pdf(id): deletion of a pdf file

"""
import pdfminer.high_level
from pdfminer.pdfparser import PDFSyntaxError
from pdfrw import PdfParseError, PdfReader

import file_pdf as fp
import utility

session = fp.Session()
fp.Base.metadata.create_all(fp.engine)


def __extract_metadata(file_pdf):
    """
    extract metadata of the pdf file
    :rtype: dict
    :param file_pdf:
    :return: dictionary of metadata

    >>> __extract_metadata("")
    Traceback (most recent call last):
        ...
    ValueError: Could not read PDF file
    """

    try:
        reader = PdfReader(file_pdf)
        return dict(reader.Info)

    except PdfParseError as error:
        raise ValueError('Could not read PDF file') from error


def __extract_text(file_pdf):
    """
    extract text content of the pdf file
    :rtype: str
    :param file_pdf:
    :return: text content

    >>> __extract_text("")
    Traceback (most recent call last):
        ...
    ValueError: Could not extract text from PDF file

    >>> __extract_text("test/test1.pdf")
    'Ceci est un pdf de test \\n\\n\\x0c'
    """

    try:
        text = pdfminer.high_level.extract_text(file_pdf)
        return text

    except PDFSyntaxError as error:
        raise ValueError('File type seems not to be PDF') from error
    except FileNotFoundError as error:
        raise ValueError('Could not extract text from PDF file') from error


@utility.debug(__name__)
def create_pdf(filename):
    """
    Create instance of FilePDF in database
    :rtype: int
    :param filename:
    :return: id
    """
    file = fp.FilePDF(title=filename)
    session.add(file)
    session.commit()

    return file.id


@utility.debug(__name__)
def process_pdf(file_pdf, identifiant):
    """
    refresh in database metadata and text content of the pdf file
    :param identifiant:
    :param file_pdf:
    """
    file = get_file_pdf(identifiant)
    try:
        file.text = __extract_text(file_pdf)
        param = __extract_metadata(file_pdf)
        for param_unit in param:
            item = fp.Metadata(key=param_unit[1:],
                               value=param[param_unit][1:-1])
            file.metadatas.append(item)
        file.processing_state = 'success'
    except ValueError as error:
        print(error)
        file.processing_state = 'failure'
    finally:
        session.commit()


def __list_pdf():
    """
    print all saved pdf files
    """
    for instance in session.query(fp.FilePDF):
        print(instance)


@utility.debug(__name__)
def get_file_pdf(identifiant) -> fp.FilePDF:
    """
    search a pdf file from its id
    :rtype: fp.FilePDF
    :param identifiant: str
    :return: return the pdf file corresponding to the id

    >>> get_file_pdf(-1)
    Traceback (most recent call last):
        ...
    ValueError: id must be a positive int

    import sys    >>> __get_file_pdf(sys.maxsize-1)
    Traceback (most recent call last):
        ...
    ValueError: Unknown ID
    """

    if isinstance(identifiant, str) and identifiant.isnumeric:
        identifiant = int(identifiant)

    if (not isinstance(identifiant, int)) or (identifiant < 0):
        raise ValueError('id must be a positive int')

    query = session.query(fp.FilePDF).filter(fp.FilePDF.id == identifiant)

    if not session.query(query.exists()):
        raise ValueError('Unknown ID')

    return query.first()


@utility.debug(__name__)
def read_txt(identifiant):
    """
    return the content text of the pdf file corresponding to the id
    :rtype: str
    :param identifiant:
    :return: content text

    >>> read_txt('-1')
    Traceback (most recent call last):
        ...
    ValueError: id must be a positive int

    import sys    >>> read_txt(sys.maxsize-1)
    Traceback (most recent call last):
        ...
    ValueError: Unknown ID
    """
    try:
        file_pdf = get_file_pdf(identifiant)
        if file_pdf is None:
            raise ValueError('Unknown ID')
        return file_pdf.text

    except ValueError as error:
        raise ValueError(error) from error


@utility.debug(__name__)
def read_metada(identifiant):
    """
    return the metadata of the pdf file corresponding to the id
    :rtype: dict
    :param identifiant:
    :return: dictionary of metadata

    >>> read_metada('-1')
    Traceback (most recent call last):
        ...
    ValueError: id must be a positive int

    import sys    >>> read_metada(sys.maxsize-1)
    Traceback (most recent call last):
        ...
    ValueError: Unknown ID
    """
    try:
        file_pdf = get_file_pdf(identifiant)
        if file_pdf is None:
            raise ValueError('Unknown ID')
        return file_pdf.metadatas
    except ValueError as error:
        raise ValueError(error) from error


@utility.debug(__name__)
def read_status(identifiant):
    """
        return the processing_state of the pdf file corresponding to the id
        :rtype: string
        :param identifiant:
        :return: processing_state

        >>> read_status('-1')
        Traceback (most recent call last):
            ...
        ValueError: id must be a positive int

        import sys    >>> read_metada(sys.maxsize-1)
        Traceback (most recent call last):
            ...
        ValueError: Unknown ID
        """
    try:
        file_pdf = get_file_pdf(identifiant)
        if file_pdf is None:
            raise ValueError('Unknown ID')
        return file_pdf.processing_state
    except ValueError as error:
        raise ValueError(error) from error


@utility.debug(__name__)
def delete_pdf(identifiant):
    """
        delete the pdf file corresponding to the id
        :rtype: string
        :param identifiant:
        :return: status of delete

        >>> delete_pdf('-1')
        Traceback (most recent call last):
            ...
        ValueError: id must be a positive int

        import sys    >>> read_metada(sys.maxsize-1)
        Traceback (most recent call last):
            ...
        ValueError: Unknown ID
        """
    try:
        file_pdf = get_file_pdf(identifiant)
        if file_pdf is None:
            raise ValueError('Unknown ID')
        session.delete(file_pdf)
        session.commit()
        return True
    except ValueError as error:
        raise ValueError(error) from error
